# Name: Joey Carberry
# Date: Jan 25, 2016
# Project: Final Exam

import math
class Circle2D:
    def __init__(self, x, y, radius):
        # x and y variables show the center of the circle in an x and y position
        self.__x = x
        self.__y = y
        self.__radius = radius

    def get_x(self):
        return self.__x

    def get_y(self):
        return self.__y

    def get_radius(self):
        return self.__radius

    def set_x(self, x):
        self.__x = x

    def set_y(self, y):
        self.__y = y

    def set_radius(self, radius):
        self.__radius = radius

    def get_area(self):
        # area = (pi)(radius^2)
        self.area = math.pi * (math.pow(self.__radius, 2))
        return self.area

    def get_perimeter(self):
        # perimeter = (2)(pi)(radius)
        self.perimeter = (2 * math.pi * self.__radius)
        return self.perimeter

    def contains_point(self, x_point, y_point):
        value = math.pow((x_point - self.__x), 2) + math.pow((y_point - self.__y), 2)
        if value < (math.pow(self.__radius, 2)):
            return 'Point is within the circle.'
        else:
            return 'Point is not within the circle.'

    def contains(self, x2, y2, r2):
        # Find the distance between the points.
        self.value1 = math.pow((x2 - self.__x), 2) + math.pow((y2 - self.__y), 2)
        self.distance = math.sqrt(self.value1)
        if self.distance > (self.__radius + r2):
            return 'The Circles do not overlap'
        elif self.distance <= math.fabs(self.__radius - r2):
            return 'The Second Circle is entirely inside the First'

    def overlaps(self, r2):

        if self.distance <= math.fabs(self.__radius + r2):
            return 'The Circles overlap'
        else:
            return 'The Circles do not overlap'

'''
userX = float(input('What is the x value? '))
userY = float(input('What is the y value? '))
userRadius = int(input('What is the radius? '))

userCircle = Circle2D(userX, userY, userRadius)
print('Area: ', userCircle.get_area())
print('Perimeter: ', userCircle.get_perimeter())

userXPoint = float(input('What is the x point? '))
userYPoint = float(input('What is the y point? '))
print(userCircle.contains_point(userX, userY, userXPoint, userYPoint))

userX2 = float(input('What is the second x value? '))
userY2 = float(input('What is the second y value? '))
userRadius2 = float(input('What is the second radius? '))
print(userCircle.contains(userX, userX2, userY, userY2, userRadius2))

print(userCircle.overlaps(userRadius2))
'''