# Name: Joey Carberry
# Date: Jan 25, 2016
# Project: Final Exam

import circle2d
c1 = circle2d.Circle2D(2, 2, 5.5)
c2 = circle2d.Circle2D(4, 5, 10.5)
c3 = circle2d.Circle2D(3, 5, 2.3)
print('=== c1 ===')
print('Area: ', c1.get_area())
print('Perimeter: ', c1.get_perimeter())
print(c1.contains_point(3, 3))
print(c1.contains(4, 5, 10.5))
print(c1.overlaps(2.3) + '\n')

print('=== c2 ===')
print('Area: ', c2.get_area())
print('Perimeter: ', c2.get_perimeter())

print('=== c3 ===')
print('Area: ', c3.get_area())
print('Perimeter: ', c3.get_perimeter())
